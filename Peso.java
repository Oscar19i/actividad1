public class Peso{
private float libras;
private float onzas;
private float kilos;



public Peso(){
	libras = 0f;
	onzas = 0f;
	kilos = 0f;

}


public float convOzKl(float dato){
	kilos = 0.02f * dato;
	return kilos;
}

public float getOzKl(){
	return kilos;
}

public float convOzL(float dato){
	libras = 0.06f * dato;
	return libras;
}

public float getOzL(){
	return libras;
}

public float convLibOZ(float dato){
	libras = 16 * dato;
	return libras;
}

public float getLibOZ(){
	return libras;
}

public float convLibKil(float dato){
	libras = 0.45f * dato;
	return libras;
}

public float getLibKil(){
	return libras;
}

public float convKilOz(float dato){
	kilos = 35.27f * dato;
	return kilos;
}

public float getKilOz(){
	return kilos;
}

public float convKilLib(float dato){
	kilos =  2.20f * dato;
	return kilos;
}

public float getKilLib(){
	return kilos;
}

}
