import java.io.*;
public class principalPeso{
public static void main(String args[])throws IOException{
boolean flag = true;
float pes = 0f;
int opc = 0;

Peso convertidor = new Peso();
//System.out.println("");

InputStreamReader ent = new InputStreamReader (System.in);
BufferedReader al = new BufferedReader(ent);

while(flag){
	System.out.println("\nPrograma para converir unidades de peso.");
	System.out.println("Elija una opcion");
	System.out.println("1.-Onzas a Kilos");
	System.out.println("2.-Onzas a Libras");
	System.out.println("3.-Libras a Onzas");
	System.out.println("4.-Libras a Kilos");
	System.out.println("5.-Kilos a Onzas");
	System.out.println("6.-Kilos a Libras");
	System.out.println("7.-Salir");
	opc = Integer.parseInt(al.readLine());

	switch(opc){
		case 1:
		System.out.print("Conversor de Onzas a kilos, por favor escriba el peso en onzas: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convOzKl(pes);
		System.out.print(pes + "oz = " + convertidor.getOzKl() + "Kg");
		System.out.println("");
		System.out.println("");

		break;

		case 2:
		System.out.print("Conversor de Onzas a Libras, por favor escriba el peso en onzas: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convOzL(pes);
		System.out.print(pes + "oz = " + convertidor.getOzL() + "Lb");
		System.out.println("");
		System.out.println("");
		break;


		case 3:
		System.out.print("Conversor de Libras a Onzas, por favor escriba el peso en Libras: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convLibOZ(pes);
		System.out.print(pes + "Lb = " + convertidor.getLibOZ() + "Oz");
		System.out.println("");
		System.out.println("");
		break;

		case 4:
		System.out.print("Conversor de Libras a Kilos, por favor escriba el peso en Libras: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convLibKil(pes);
		System.out.print(pes + "Lb = " + convertidor.getLibKil() + "Kg");
		System.out.println("");
		System.out.println("");
		break;


		case 5:
		System.out.print("Conversor de Kilos a Onzas, por favor escriba el peso en Kilos: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convKilOz(pes);
		System.out.print(pes + "KG = " + convertidor.getKilOz() + "-oz");
		System.out.println("");
		System.out.println("");
		break;

		case 6:
		System.out.print("Conversor de Kilos a Libras, por favor escriba el peso en Kilos: ");
		pes = Float.parseFloat(al.readLine());
		convertidor.convKilLib(pes);
		System.out.print(pes + "Kg = " + convertidor.getKilLib() + "Lb");
		System.out.println("");
		System.out.println("");
		break;

		case 7:
		flag = false;
		break;


	}


}




}
}

